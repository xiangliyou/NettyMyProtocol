package server.common.code;

import io.netty.handler.codec.marshalling.*;
import org.jboss.marshalling.MarshallerFactory;
import org.jboss.marshalling.Marshalling;
import org.jboss.marshalling.MarshallingConfiguration;

import io.netty.handler.codec.marshalling.UnmarshallerProvider;


/**
 * Created by xiangliyou on 17-9-22.
 *
 * @DESCRIPTION
 *      定义MarshallingCodeCFactory工厂类来获取JBoss Marshalling 类
 */
public class MarshallingCodeCFactory {

    /**
     * 获取解码类
     * @return
     */
    public static NettyMarshallingDecoder buildMarshallingDecoder() {
        MarshallerFactory marshallerFactory = Marshalling.getProvidedMarshallerFactory("serial");
        MarshallingConfiguration configuration = new MarshallingConfiguration();
        configuration.setVersion(5);
        UnmarshallerProvider provider = new DefaultUnmarshallerProvider(marshallerFactory, configuration);

        return new NettyMarshallingDecoder(provider, 1024);
    }

    /**
     * 获取编码类
     * @return
     */
    public static NettyMarshallingEncoder buildMarshallingEncoder() {
        MarshallerFactory marshallerFactory = Marshalling.getProvidedMarshallerFactory("serial");
        MarshallingConfiguration configuration = new MarshallingConfiguration();
        configuration.setVersion(5);
        MarshallerProvider provider = new DefaultMarshallerProvider(marshallerFactory, configuration);

        return new NettyMarshallingEncoder(provider);
    }

}
