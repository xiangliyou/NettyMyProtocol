package server.common.message;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by xiangliyou on 17-9-24.
 *
 * @DESCRIPTION
 *    各种类型的消息工厂
 */
public class NettyMessageFactory {

    //注：byte能表示的数字范围是 -128~127
    //握手认证请求码
    public static final byte MESSAGE_TYPE_LOGIN_AUTH_REQ_CODE = 1;
    //握手认证应答码
    public static final byte MESSAGE_TYPE_LOGIN_AUTH_RES_CODE = 2;
    //ping消息，客户端发出的心跳的数据包标记
    public static final byte PING_MSG = 3;
    //pong消息，服务端回应客户端的心跳的数据包标记
    public static final byte PONG_MSG = 4;

    public static final byte MESSAGE_TYPE_OTHER_CODE = 127;




    //握手认证应答的结果——成功
    public static final byte MESSAGE_TYPE_LOGIN_AUTH_RESULT_SUCCESS = 0;
    //握手认证应答的结果——失败
    public static final byte MESSAGE_TYPE_LOGIN_AUTH_RESULT_FAILURE = -1;


    /**
     * 握手请求
     */
    public static NettyMessage buildLoginReq() {
        NettyMessage message = new NettyMessage();
        //消息头
        Header header = new Header();
        header.setType(MESSAGE_TYPE_LOGIN_AUTH_REQ_CODE);
        message.setHeader(header);
        return message;
    }

    /**
     * 握手应答
     * @param result 握手的结果
     */
    public static NettyMessage buildLoginResponse(final byte result) {
        NettyMessage message = new NettyMessage();
        Header header = new Header();
        header.setType(MESSAGE_TYPE_LOGIN_AUTH_RES_CODE);
        message.setHeader(header);
        message.setBody(result);
        return message;
    }

    //构建一个

    /**
     * 获取一个心跳包
     * @param hearType  心跳类型 NettyMessageFactory.PING_MSG 或者 PONG_MSG
     * @param heartbeatCount 心跳次数
     * @return
     */
    public static NettyMessage buildHeartBag(final byte hearType, final int heartbeatCount) {
        NettyMessage message = new NettyMessage();
        Header header = new Header();
        header.setType(MESSAGE_TYPE_LOGIN_AUTH_RES_CODE);
        message.setHeader(header);
        message.setBody("heartbeatCount:"+heartbeatCount);
        return message;
    }


    /**
     * 其他的一些消息
     * @param body 消息体
     */
    public static NettyMessage build(final Object body) {
        //TODO
        NettyMessage message = new NettyMessage();
        Header header = new Header();
        header.setType(MESSAGE_TYPE_OTHER_CODE);
        header.setSessionID(100L);
        //TODO 具体包头参数的配置.....
        message.setHeader(header);
        message.setBody(body);

        return message;
    }


}
