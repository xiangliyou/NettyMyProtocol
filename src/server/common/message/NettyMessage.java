package server.common.message;

/**
 * Created by xiangliyou on 17-9-22.
 *
 * @DESCRIPTION
 *      私有协议栈的消息
 */
public final class NettyMessage {

    //消息头
    private Header mHeader;
    //消息体
    private Object mBody;

    public final Header getHeader() {
        return mHeader;
    }

    public final void setHeader(Header header) {
        mHeader = header;
    }

    public final Object getBody() {
        return mBody;
    }

    public final void setBody(Object body) {
        mBody = body;
    }

    @Override
    public String toString() {
        return "NettyMessage{" +
                "mHeader=" + mHeader +
                ", mBody=" + mBody +
                '}';
    }
}
