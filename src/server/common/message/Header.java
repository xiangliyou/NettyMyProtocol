package server.common.message;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xiangliyou on 17-9-22.
 *
 * @DESCRIPTION
 *      发送的每个消息的消息头
 */
public final class Header {
    //netty消息校验码    0xABEF固定头+主版本号+次版本号
    private int crcCode = 0xABEF0101;
    //消息长度
    private int length = 0;
    //会话ID
    private long sessionID = -1;
    //消息类型
    private byte type = -127;
    //消息优先级
    private byte priority = -127;
    //附件
    private Map<String, Object> attachment = new HashMap<>();

    public Header() {
    }

    public int getCrcCode() {
        return crcCode;
    }

    public void setCrcCode(int crcCode) {
        this.crcCode = crcCode;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public long getSessionID() {
        return sessionID;
    }

    public void setSessionID(long sessionID) {
        this.sessionID = sessionID;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public Map<String, Object> getAttachment() {
        return attachment;
    }

    public void setAttachment(Map<String, Object> attachment) {
        this.attachment = attachment;
    }


    @Override
    public String toString() {
        return "Header{" +
                "crcCode=" + crcCode +
                ", length=" + length +
                ", sessionID=" + sessionID +
                ", type=" + type +
                ", priority=" + priority +
                ", attachment=" + attachment +
                '}';
    }


    private Header(Builder builder) {
        setCrcCode(builder.crcCode);
        setLength(builder.length);
        setSessionID(builder.sessionID);
        setType(builder.type);
        setPriority(builder.priority);
        setAttachment(builder.attachment);
    }

    public static final class Builder {
        private int crcCode;
        private int length;
        private long sessionID;
        private byte type;
        private byte priority;
        private Map<String, Object> attachment;

        public Builder() {
        }

        public Builder crcCode(int val) {
            crcCode = val;
            return this;
        }

        public Builder length(int val) {
            length = val;
            return this;
        }

        public Builder sessionID(long val) {
            sessionID = val;
            return this;
        }

        public Builder type(byte val) {
            type = val;
            return this;
        }

        public Builder priority(byte val) {
            priority = val;
            return this;
        }

        public Builder attachment(Map<String, Object> val) {
            attachment = val;
            return this;
        }

        public Header build() {
            return new Header(this);
        }
    }
}
