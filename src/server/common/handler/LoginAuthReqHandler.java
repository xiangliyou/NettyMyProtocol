package server.common.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import server.common.message.NettyMessage;
import server.common.message.NettyMessageFactory;

/**
 * Created by xiangliyou on 17-9-22.
 *
 * @DESCRIPTION 客户端的握手请求和收到握手请求的处理
 * 在服务端和客户端的tcp链路建立成功，通道激活时，握消息的介入和安全认证的处理
 */
public class LoginAuthReqHandler extends ChannelInboundHandlerAdapter {

    /**
     * 客户端与服务端在tcp三次握手成功后，由客户端构造握手请求给服务端
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(NettyMessageFactory.buildLoginReq());
    }

    /**
     * 对握手应答消息的处理，如果不是握手应答消息，透传给后面的ChannelHandler处理
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        NettyMessage message = (NettyMessage) msg;
        //如果是握手应答消息，需要判断是否认证成功
        if (message.getHeader() != null
                && message.getHeader().getType() == NettyMessageFactory.MESSAGE_TYPE_LOGIN_AUTH_RES_CODE) {

            //如果结果非0，说明认证失败，关闭链路，重新发起连接请求
            byte loginResult = (byte) message.getBody();
            if (loginResult != NettyMessageFactory.MESSAGE_TYPE_LOGIN_AUTH_RESULT_SUCCESS) {
                //握手失败,关闭连接
                System.out.println("Login is failure:" + message);
                ctx.close();
            } else {
                System.out.println("Login is OK:" + message);
            }
            //传递给心跳Handler，开始发送心跳
            ctx.fireChannelRead(msg);
        } else {
            ctx.fireChannelRead(msg);
        }
    }


    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
    }
}
