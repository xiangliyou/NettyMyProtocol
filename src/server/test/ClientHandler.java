package server.test;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import server.common.message.NettyMessage;

/**
 * Created by xiangliyou on 17-9-23.
 *
 * @DESCRIPTION 客户端事件处理
 */
public class ClientHandler extends SimpleChannelInboundHandler<NettyMessage> {


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, NettyMessage message) throws Exception {
        System.out.println("Client 收到消息" + message.toString());
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}