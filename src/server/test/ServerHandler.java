package server.test;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import server.common.message.NettyMessage;

/**
 * Created by xiangliyou on 17-9-23.
 *
 * @DESCRIPTION
 * 服务端收到心跳的处理
 */
public class ServerHandler extends SimpleChannelInboundHandler<NettyMessage> {


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, NettyMessage message) throws Exception {
        System.out.println("Server : 收到消息" + message.toString());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}